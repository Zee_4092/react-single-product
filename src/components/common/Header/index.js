import HeaderTop from "./HeaderTop";
import HeaderNav from "./HeaderNav";
import "./style.scss";

function Header() {
    
    return (
        <header className="site-header">

            <HeaderTop/>
            <HeaderNav/>

        </header>
    )

}

export default Header;
