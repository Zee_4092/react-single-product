import './style.scss'
import { useState } from 'react'
import HiddenInfoBlock from "components/HiddenInfoBlock"

const HeaderTop = () => {

    const [show, setShow] = useState(false)

    return (
        <div className="header-top flex align-center bg-primary site-padding color-white text-center">
            <h2 onClick={() => setShow(true)} className="header-top__title flex align-center justify-between no-margin font-normal relative"><span className="hide-sm">Enjoy complimentary carbon neutral shipping on all orders. </span> Express delivery now available.<span className="header-top__plus">+</span></h2>
            <HiddenInfoBlock show={show} setShow={setShow} />
        </div>
    )
    
}

export default HeaderTop