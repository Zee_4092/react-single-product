import './style.scss'

const HiddenInfoBlock = (props) => {

    const bg = props.show ? 'block' : 'hide'
    const classes = `info-block-bg block full-width full-height ${bg}`

    return (
         
        <>
            <aside style={props.show ? { transform: 'translate3d(0, 0, 0)' } : {}} className="info-block bg-body full-width full-height flex flex-column justify-center">
                <button onClick={() => props.setShow(false)} className="header-top__info__btn-cross btn-cross absolute"><span></span><span></span></button>
                {props.text && <div className="overflow-y-auto" dangerouslySetInnerHTML={{ __html: props.text }}></div>}
            </aside>
            <span onClick={() => props.setShow(false)} className={classes}></span>
        </>
        
    )
}

export default HiddenInfoBlock