import { useState, useContext } from 'react'
import { cartListContext } from 'context/CartListProvider';
import './style.scss'

const Dropdown = (props) => {

    // Qty
    const [showQty, setShowQty] = useState(false)
    const showUL = showQty ? 'block' : 'hide'
    const classes = `dropdown__ul list-style-none absolute flex flex-column align-center full-width ${showUL}`

    const showQtySelection = () => {
        setShowQty(true)
        document.addEventListener("mousedown", closeSelection)
    }

    const [cartList, setCartList] = useContext(cartListContext)

    const closeSelection = (e) => {

        const exist = cartList.find(item => (item.id === props.item.id))

        if (e.target.classList.contains('dropdown-li') && !isNaN(+e.target.innerText)) // checking class also very important
            setCartList(cartList.map(item => (item.id === props.item.id && (!isNaN(+e.target.innerText))) ? { ...exist, qty: +e.target.innerText } : item))
        
        
        setShowQty(false)
        document.removeEventListener("mousedown", closeSelection)

    }


    return (
        <div onClick={showQtySelection} className="dropdown relative flex align-center color-white relative">
            {props.item.qty}
            <span className="dropdown__arrow absolute"><span></span><span></span></span>
            <ul className={classes}>
                {[...Array(props.item.stock_qty)].map((val, index) => <li className="dropdown-li full-width flex justify-center" key={index + 1}>{index + 1}</li>)}
            </ul>
        </div>
    )

}

export default Dropdown