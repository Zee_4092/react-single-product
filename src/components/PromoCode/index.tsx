import './style.scss'
import { useState, useRef, useEffect } from 'react'

const PromoCode = () => {

    // Promo
    const [showPromo, setShowPromo] = useState(false)
    const showPromotion = showPromo ? 'flex' : 'hide'
    const classes = `promo__slide full-width full-height ${showPromotion}`

    const input:any = useRef(null)

    useEffect(() => {
        input.current.focus()
    }, [showPromo])


    return (
        <section className="promo flex align-center justify-between">
            {/* style={props.cartOpenState ? { transform: 'translate3d(0,0,0)' } : { transform: 'translate3d(0,-100%,0)' }} */}
            <div className={classes}>
                <span onClick={() => setShowPromo(false)} className="promo-bg block full-width full-height absolute"></span>
                <div className="promo__slide__inner bg-body site-padding full-width relative">
                    <button onClick={() => setShowPromo(false)} className="promo__slide__btn-cross btn-cross absolute">
                        <span></span><span></span>
                    </button>
                    <h4 className="promo__slide__title font-normal">Apply a promotional code</h4>
                    <label className="promo__label block" htmlFor="promocode">
                        <span className="promo__label__text block">Promotional code</span>
                        <input ref={input} className="promo__label__input transparent block full-width" type="text" id="promocode" />
                    </label>
                    <button className="btn full-width">Apply</button>
                 </div>
            </div>
            
            <button onClick={() => setShowPromo(true)} className="promo__btn-text transparent color-white">Apply a promotional code</button>
            <button onClick={() => setShowPromo(true)} className="promo__btn-plus btn-plus relative circle flex justify-center align-center transparent"><span></span><span></span></button>
        </section>
    )

}

export default PromoCode