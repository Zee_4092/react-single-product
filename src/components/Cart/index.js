import './style.scss'
import PromoCode from 'components/PromoCode'
import Dropdown from 'components/Dropdown';
import { cartListContext } from 'context/CartListProvider';
import { useContext, useEffect, useState } from 'react'

const Cart = (props) => {
    
    
    // getting cart data
    const [cartList, setCartList] = useContext(cartListContext)

    const [total, setTotal] = useState(0)

    const removeItem = id => {
        setCartList(cartList.filter(item => item.id !== id))
        if (cartList.length === 1) {
            props.setCartOpenState(false)
        }
    }

    useEffect(() => {
        setTotal(
            cartList.reduce((a, c) => a + c.price * c.qty, 0)
        )
    },[cartList])

    return (
        <aside style={props.cartOpenState ? { transform: 'translate3d(0,0,0)' } : { transform: 'translate3d(0,-100%,0)'}} className="cart overflow-y-auto full-width full-height">
            
            {(cartList && cartList.length === 0) && <div className="no-item-msg color-white">You have no items in your cart</div>}
            
            {(cartList && cartList.length > 0) && <div className="cart__inner flex flex-column justify-between">
                <div className="cart__inner-top">

                    <div className="cart__block cart__block--header flex justify-between">
                        <div className="item-width-25">Cart</div>
                        <div className="item-width-25 hide-sm">Size</div>
                        <div className="item-width-25 hide-sm">Quantity</div>
                        <button onClick={() => props.setCartOpenState(false)} className="cart__btn-cross btn-cross relative"><span></span><span></span></button>
                    </div>
                    
                    {cartList.map((item, index) => 
                        
                        <div key={index} className="cart__block cart__block--cart-item">
                            <div className="cart__block--cart-item__inner flex justify-between align-center relative">
                                <h3 className="cart__block--cart-item__title item-width-25 color-white font-normal no-margin">{item.title}</h3>
                                <div className="cart__block--cart-item__details item-width-25 hide-sm">{item.size}</div>
                                <div className="relative item-width-25 flex align-center">

                                    <Dropdown item={item} />

                                    <button onClick={() => removeItem(item.id)} className="cart__block--cart-item__remove-btn-l absolute hide-sm transparent">Remove</button>
                                </div>
                                <span className="cart__block--cart-item__inner__span hide-sm"></span>
                                <div className="cart__block--cart-item__price hide-sm">£{(item.price * item.qty).toFixed(2)}</div>
                            </div>
                            <div className="flex justify-between align-center">
                                <div>
                                    <div className="cart__block--cart-item__details hide-l">{item.size}</div>
                                    <button onClick={() => removeItem(item.id)} className="cart__block--cart-item__remove-btn hide-l transparent">Remove</button>
                                </div>
                                <div className="cart__block--cart-item__price hide-l">£{(item.price * item.qty).toFixed(2)}</div>
                            </div>
                        </div>
                    )}

                    <PromoCode />
                </div>

                {(cartList && cartList.length > 0) && <div className="cart__inner__bottom">
                    <p className="cart__inner__bottom__p">Shipping from the United Kingdom.</p>

                    <div className="cart__checkout">
                        <p className="cart__checkout__p no-margin">Enjoy complimentary shipping on all orders.</p>
                        <div className="cart__checkout__total full-width flex justify-between align-center">
                            <div>Subtotal (Tax Incl.)</div>
                            <div className="cart__checkout__total__price color-white">£{total.toFixed(2)}</div>
                        </div>
                        <button className="cart__checkout__btn btn full-width">Checkout</button>
                    </div>

                </div>}
            </div>}
            <span onClick={() => props.setCartOpenState(false)} className="cart-bg full-width full-height block absolute"></span>
        </aside>
    )

}

export default Cart