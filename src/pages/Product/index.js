import "./style.scss"
import { Link } from 'react-router-dom'
import { useState, useContext } from 'react'
import { cartListContext } from 'context/CartListProvider'
import HiddenInfoBlock from "components/HiddenInfoBlock"

const Product = () => {

    const product = {
        "category": "Skin",
        "subcategory": "Cleanse",
        "title": "In Two Minds Facial Cleanser",
        "description": "A gentle gel-based formulation that cleanses thoroughly without drying the skin or stripping its natural oils.",
        "ingredients": "<strong>Ingredients</strong><p>Water(Aqua), Sodium Laurylglucosides Hydroxypropylsulfonate, Cocamidopropyl Hydroxysultaine, PEG- 40 Hydrogenated Castor Oil, Sorbitol, Hamamelis Virginiana (Witch Hazel) Water, Sodium Methyl Cocoyl Taurate, Sodium Chloride, Phenoxyethanol, PEG - 120 Methyl Glucose Dioleate, Polysorbate 80, Lavandula Angustifolia(Lavender) Oil, Sodium Dehydroacetate, Sodium Gluconate, Ormenis Multicaulis Oil, Rosmarinus Officinalis(Rosemary) Leaf Oil, Citric Acid, Salicylic Acid, Coconut Acid, Salvia Officinalis(Sage) Oil, Benzoic Acid, Tocopherol, Linalool, Limonene.</p><p>This ingredient list is subject to change.Prior to use, please refer to the product label for the most accurate information.</p><p>All Aesop products are vegan.We only use ingredients with a proven record of safety, efficacy and sustainability.</p>",
        "sizes": [
            {
                "id": 1,
                "size": "100 ml",
                "price": 23,
                "stock_qty": 4
            },
            {   
                "id": 2,
                "size": "200 ml",
                "price": 35,
                "stock_qty": 4
            }
        ],
        "image": "https://www.aesop.com/u1nb1km7t5q7/7tnEMRS4AaGorVjN4Qh6iN/77dd7f1107054d94f3bdc1a845b083d8/Aesop-Skin-In-Two-Minds-Facial-Cleanser-100mL-Large-835x962px.png",
    }
    
    // style
    const [show, setShow] = useState(false)
    
    // form default values
    const [size, setSize] = useState(product.sizes[0].size)
    const [price, setPrice] = useState(product.sizes[0].price)
    const [id, setId] = useState(product.sizes[0].id)
    const [stockQty, setStockQty] = useState(product.sizes[0].stock_qty)

    const cartItem = { // new cart object
        "id": id,
        "title": product.title,
        "size": size,
        "price": price,
        "qty": 1,
        "stock_qty": stockQty
    }

    const [cartList, setCartList] = useContext(cartListContext)

    const addToCart = (e) => {

        e.preventDefault() // prevent refresh

        const exist = cartList.find(item => (item.id === cartItem.id))

        if(exist) {
            setCartList(
                cartList.map(item => item.id === cartItem.id ? {...exist, qty: exist.qty + 1} : item)
            )
        } else {
            setCartList([...cartList, {...cartItem, qty: 1}])
        }

    }

     // form funcs
    const onSizeChange = (e, price, id, stockQty) => {
        setSize(e.target.value)
        setPrice(price)
        setId(id)
        setStockQty(stockQty)
    }


    return (
        <main className="product site-padding">

            <div className="product__breadcrumb hide-xl"> 
                <ul className="flex no-margin">
                    <li><Link to="#" className="underline-animation">{product.category}</Link></li>
                    <li><Link to="#" className="underline-animation">{product.subcategory}</Link></li>
                </ul>
            </div>
            <div className="product__desktop-breakpoint">
                <picture className="product__picture flex align-center justify-center">
                    <source media="(min-width: 1025px)" srcSet="https://www.aesop.com/u1nb1km7t5q7/7tnEMRS4AaGorVjN4Qh6iN/77dd7f1107054d94f3bdc1a845b083d8/Aesop-Skin-In-Two-Minds-Facial-Cleanser-100mL-Large-835x962px.png" />
                    <source media="(min-width: 640px)" srcSet="https://www.aesop.com/u1nb1km7t5q7/1qBFy7BYzet5LV546uIUtv/da3ecdcc7189e90e998015a60557d4ca/Aesop-Skin-In-Two-Minds-Facial-Cleanser-100mL-Medium-653x752px.png" />
                    <source media="(min-width: 0px)" srcSet="https://www.aesop.com/u1nb1km7t5q7/4pw7rNvMld15T98jBr2zjR/72ee92376ae6ef7c91afbad48621a996/Aesop-Skin-In-Two-Minds-Facial-Cleanser-100mL-Small-455x524px.png" />
                    <img alt="In Two Minds Facial Cleanser in amber glass bottle" src={product.image} />
                </picture>
                <div className="product__breakpoint">
                    <div>
                        <div className="product__breadcrumb hide-sm-l">
                            <ul className="flex no-margin">
                                <li><Link to="#" className="underline-animation">{product.category}</Link></li>
                                <li><Link to="#" className="underline-animation">{product.subcategory}</Link></li>
                            </ul>
                        </div>
                        <h1 className="product__title font-normal">{product.title}</h1>
                        <p className="product__description">{product.description}</p>
                        <div className="product__details product__details--border-top">
                            <strong>Suited to</strong>
                            <p className="no-margin">Combination skin</p>
                        </div>
                        <div className="product__details">
                            <strong>Skin feel</strong>
                            <p className="no-margin">Combination skin</p>
                        </div>

                        <div className="product__details relative">
                            <strong>Key ingredients</strong>
                            <p className="no-margin">Witch Hazel, Salicylic Acid, Sage Leaf</p>
                            <button onClick={() => setShow(true)} className="product__details__btn btn-plus transparent absolute circle flex align-center justify-center"><span></span><span></span></button>

                            <HiddenInfoBlock text={product.ingredients} show={show} setShow={setShow}/>
                        </div>

                    </div>
                    <div>
                        <form onSubmit={addToCart} className="product__form">
                            <p>Sizes:</p>

                            {product.sizes.map((item, index) => (
                                <label key={index} htmlFor="millilitre">
                                    <input defaultChecked={index === 0} onChange={(val) => onSizeChange(val, item.price, item.id, item.stock_qty)} type="radio" name="millilitre" value={item.size} />{item.size}
                                </label>
                            ))}

                            <button className="product__form__btn btn block full-width">ADD TO CART - <span>£{price}</span></button>

                        </form>
                        <p className="product__pay">Pay in 30 days with <b>Klarna</b>. No fees. <span className="underline">Learn more</span></p>
                    </div>
                    <div>
                        <p><b>Discover the kit</b><br />
                            Balance: Classic Skin Care Kit</p>
                    </div>
                </div>
            </div>

        </main>
    )
}

export default Product