import { Link } from 'react-router-dom';

// localStorage.setItem("wishlist", JSON.stringify(localStorageList))

const Home = () => {
    return (
        <div className="flex align-center justify-center flex-column text-center">
            <Link to="/product">
                <h2>
                    Click here to go to the single product page.
                </h2>
            </Link>
        </div>
    );
}

export default Home;
