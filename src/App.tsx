import {BrowserRouter, Routes, Route} from "react-router-dom"
// context
import CartListProvider from 'context/CartListProvider';
//Header
import Header from "./components/common/Header"
//Pages
import Home from "./pages/Home"
import Product from "./pages/Product/index.js"

const App = () => {
  return (
    <BrowserRouter basename={'/files/react'}>
      <CartListProvider>
        <Header />
        <Routes>
          <Route path="/" element={<Home />} /> 
          <Route path="/product" element={<Product />} />
        </Routes>
      </CartListProvider>
    </BrowserRouter>
  )
}

export default App

