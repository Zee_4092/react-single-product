
import { createContext, useState } from 'react'

//create a context, with createContext api
export const cartListContext = createContext()

const CartListProvider = (props) => {
    // this state will be shared with all components
 
    const [cartList, setCartList] = useState([])

    console.log(cartList)

    return (
        // this is the provider providing state
        <cartListContext.Provider value={[cartList, setCartList]}>
            {props.children}
        </cartListContext.Provider>
    )
}

export default CartListProvider