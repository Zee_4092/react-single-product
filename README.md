# The 'WISHLIST' projects contains:

- Two Route/Pages Components: Home, Wishlist, Wishlist2 (see /pages directory)

- One Header Component which further breaks down to 4 subcompoents: HeaderTop, HeaderBrand, HeaderNav, HeaderBottom (see /components/common/) *there is also a Footer component here but no styling, it was necessary to include to support 'layout' styling in main.scss*

- One ProductList Component (see components/)

- Product interface for type checking (see /interface)

- Animation.js for the purpuse of building an Async Animation libary. Currently there is one animation is registered which is imported in the TopHeader component (see /composables/Animation.js)

- Request.tsx a global function for http request + db.json (see /composables/Request.tsx and /public/db.json)

- Main style.scss & variables.scss / The main.scss has 3 parts: "prefix", "layout", "classes". The variables inclused native css variables as opposed to scss vars.

------------------------------------------------------------------------------------

 More about the Wishlist Route and ProductList Component

- The homepage includes 2 links: first link goes to the Wishlist page and the other links to Wishlist2 page: The two demo pages look exactly the same. The only difference is the way the items are loaded. Wishlist page is fetching the data from db.json and Wishlist2 is getting the data from the localstorage.
- The 'wishlist' array is passed down in the ProductList component as props along with an optional btnClose prop. (Developing the ProductList component the reuseality was in my mind: to use it any pages where the products are organised in grids)
- The product includes image, name, new with tag title, designer, size or sizes, price and retail price
- If sizes includes in the product a dropdown selection is place.
- If there is retail price along price submited, the percentage dynamicaly calculated.


------------------------------------------------------------------------------------


Running the app in dev mode:

run 'npm install' from root
then run 'npm start': (localhost:3000)

For deploying run 'npm run build' which generates the build folder to be deployed




